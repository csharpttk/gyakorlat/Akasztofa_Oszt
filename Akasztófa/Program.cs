﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Akasztófa
{
    class Program
    {
        static void Main(string[] args)
        {
            char tipp;
            Akasztófa_Alap játék = new Akasztófa_Alap();
            Console.WriteLine("{0} hibapont: {1}", játék.közbülső, játék.hibapont);
            while (játék.hibapont < 11 && játék.közbülső!=játék.kitalálandó)
            {
                Console.Write("Mi a következő tippje? ");
                tipp = Console.ReadLine()[0];
                játék = játék + tipp;
                Console.WriteLine("{0} hibapont: {1}", játék.közbülső, játék.hibapont);
            }
            if (játék.hibapont == 11)
                Console.WriteLine("Sajnos, vesztett!");
            else
                Console.WriteLine("Gratulálok!");
        }
    }
}
